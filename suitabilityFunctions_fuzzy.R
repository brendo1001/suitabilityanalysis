# FUNCTIONS FOR SUITABILITY MODELLING
  
  
  
# The wrapper function for parralel 
wrap.func<- function(sub.frame,c.mat){
  # Latzn Hypercube sample
  xmean<- sub.frame[z,1:9]
  xsd<-   sub.frame[z,10:18]
  test.lhc<- as.data.frame(lhc.samp(xmean,xsd,nsample, c.mat=c.mat))
  frost.samp<- sample(length(c(rep(0,sub.frame[z,27]*nsample),rep(1,sub.frame[z,28]*nsample))),nsample,replace=T) #Frost bznary
  frost.samp1<- c(rep(0,sub.frame[z,27]*nsample),rep(1,sub.frame[z,28]*nsample))[frost.samp] #frost
  roczs.samp<- sample(length(c(rep(0,sub.frame[z,25]*nsample),rep(1,sub.frame[z,26]*nsample))),nsample,replace=T) #rocz bznary
  roczs.samp1<- c(rep(0,sub.frame[z,25]*nsample),rep(1,sub.frame[z,26]*nsample))[roczs.samp] #rocz
  roczsOR.samp<- sample(length(c(rep(1,sub.frame[z,19]*nsample),rep(2,sub.frame[z,20]*nsample),rep(3,sub.frame[z,21]*nsample),rep(4,sub.frame[z,22]*nsample),rep(5,sub.frame[z,23]*nsample),rep(6,sub.frame[z,24]*nsample))),nsample,replace=T) #rocz ordznal
  roczsOR.samp1<- c(rep(1,sub.frame[z,19]*nsample),rep(2,sub.frame[z,20]*nsample),rep(3,sub.frame[z,21]*nsample),rep(4,sub.frame[z,22]*nsample),rep(5,sub.frame[z,23]*nsample),rep(6,sub.frame[z,24]*nsample))[roczsOR.samp] #rocz ordznal
  SD.samp<- sample(length(c(rep(0,sub.frame[z,29]*nsample),rep(1,sub.frame[z,30]*nsample))),nsample,replace=T) #sozl depth bznary
  SD.samp1<- c(rep(0,sub.frame[z,29]*nsample),rep(1,sub.frame[z,30]*nsample))[SD.samp] #sozl depth
  samp.matrzx<- cbind(test.lhc,frost.samp1,roczs.samp1,roczsOR.samp1,SD.samp1 )
  # Suitability
  run.suzt<- hazelnutSuits.fuz(samp.matrzx)
  
  col.suzt<- colMeans(run.suzt)
  
  #columnwise minimum
  r.mult<- min(col.suzt)
  
  #quantiles of results
  #q.mult.1<- quantile(r.mult, probs= c(0.05, 0.5, 0.95))
  
  #determine multi-factor issues
  #f1<- function(run.suzt){sum(run.suzt< 0.5)}
  q.mult.2<- sum(col.suzt< 0.5)
  
  
  #quantiles of results (columnwise)
  f2<- function(run.suzt){quantile(run.suzt, probs= c(0.05, 0.5, 0.95))}
  q.mult.3<- apply(run.suzt,2, f2 )
  return(list(r.mult,q.mult.2,q.mult.3[1,],q.mult.3[2,],q.mult.3[3,] ))}
  
  
#LHC function with correlation correction
lhc.samp<- function(xmean, xsd, nsample, c.mat){
  nvar=length(xmean)
  LHS.1<- LHS(factors=nvar, N=nsample,method="random")
  ran.unif<-as.matrix(LHScorcorr(LHS.1$data, COR=c.mat,eps=0.05, echo=F, maxIt=1e15))
  s<- matrix(NA, nrow= nsample, ncol= nvar)
  for (j in 1:nvar){
    s[,j] = qnorm(ran.unif[,j], mean= xmean[j], sd= xsd[j])}
  return(s)} #LHC


#matrix of paramters for fuzzy calculations
fuz.params<- matrix(NA, ncol=4, nrow= 10)
fuz.params[1,1:2]<- c(600,1200) #chill hours
fuz.params[2,1:4]<- c(7.5,10,30,50) # texture
fuz.params[3,1:2]<- c(1.5, 3.5) #drainage
fuz.params[4,1:2]<- c(0.1,0.15) # EC
fuz.params[5,1:2]<- c(40,80) # Frost
fuz.params[6,1:3]<- c(5.5,6.5,7.1) # pH
fuz.params[7,1:2]<- c(45, 50) # rainfall
fuz.params[8,1:2]<- c(30,50) # soil depth
fuz.params[9,1:4]<- c(18,20,30,35) #temperature
fuz.params[10,1:2]<- c(2,4) #rocks


### Fuzzy memberships
#HAZELNUT SUITABILITY TEST
hazelnutSuits.fuz<- function(samp.matrix){
  out.matrix<- matrix(NA, nrow=nrow(samp.matrix),ncol=10) #ten selection variable for hazelnuts
  
  # Chill Hours
  chill.dat<- samp.matrix[,1] #subset the data
  out.matrix[which(chill.dat < fuz.params[1,1]),1]<- 0.05
  out.matrix[which(chill.dat >= fuz.params[1,1] & chill.dat <= fuz.params[1,2]) ,1] <-   (((chill.dat[which(chill.dat >= fuz.params[1,1] & chill.dat <= fuz.params[1,2])] - fuz.params[1,1]) / (fuz.params[1,2] - fuz.params[1,1]))*0.95) +0.05
  out.matrix[which(chill.dat > fuz.params[1,2]),1]<- 1
  
  # Clay content
  clay.dat<- samp.matrix[,2] #subset the data
  out.matrix[which(clay.dat <= fuz.params[2,1] | clay.dat >= fuz.params[2,4] ),2]<- 0.05
  out.matrix[which(clay.dat > fuz.params[2,1] & clay.dat < fuz.params[2,2]) ,2] <-   (((clay.dat[which(clay.dat > fuz.params[2,1] & clay.dat < fuz.params[2,2])] - fuz.params[2,1]) / (fuz.params[2,2] - fuz.params[2,1]))*0.95) +0.05
  out.matrix[which(clay.dat > fuz.params[2,3] & clay.dat < fuz.params[2,4]) ,2] <-   (((fuz.params[2,4] - clay.dat[which(clay.dat > fuz.params[2,3] & clay.dat < fuz.params[2,4])]) / (fuz.params[2,4] - fuz.params[2,3]))*0.95) +0.05
  out.matrix[which(clay.dat >= fuz.params[2,2] & clay.dat <= fuz.params[2,3] ),2]<- 1
  
  
  # Soil Drainage
  drain.dat<- samp.matrix[,3] #subset the data
  out.matrix[which(drain.dat < fuz.params[3,1]),3]<- 0.05
  out.matrix[which(drain.dat >= fuz.params[3,1] & drain.dat <= fuz.params[3,2]) ,3] <-   (((drain.dat[which(drain.dat >= fuz.params[3,1] & drain.dat <= fuz.params[3,2])] - fuz.params[3,1]) / (fuz.params[3,2] - fuz.params[3,1]))*0.95) +0.05
  out.matrix[which(drain.dat > fuz.params[3,2]),3]<- 1
  
  
  # EC (transformed variable)
  ec.dat<- samp.matrix[,4] #subset the data
  out.matrix[which(ec.dat < fuz.params[4,1]),4]<- 1
  out.matrix[which(ec.dat >= fuz.params[4,1] & ec.dat <= fuz.params[4,2]) ,4] <-   (((fuz.params[4,2] - ec.dat[which(ec.dat >= fuz.params[4,1] & ec.dat <= fuz.params[4,2])] ) / (fuz.params[4,2] - fuz.params[4,1]))*0.95) +0.05
  out.matrix[which(ec.dat > fuz.params[4,2]),4]<- 0.05
  
  # Frost
  out.matrix[which(samp.matrix[,10] == 0),5]<- 1
  out.matrix[which(samp.matrix[,10] != 0 & samp.matrix[,5] > fuz.params[5,2] ),5]<- 1
  out.matrix[which(samp.matrix[,10] != 0 & samp.matrix[,5] >= fuz.params[5,1] & samp.matrix[,5] <= fuz.params[5,2]),5]<- (((samp.matrix[which(samp.matrix[,10] != 0 & samp.matrix[,5] >= fuz.params[5,1] & samp.matrix[,5] <= fuz.params[5,2]),5] - fuz.params[5,1]) / (fuz.params[5,2] - fuz.params[5,1]))*0.95) +0.05
  out.matrix[which(samp.matrix[,10] != 0 & samp.matrix[,5] < fuz.params[5,1] ),5]<- 0.05
  
  
  # pH
  ph.dat<- samp.matrix[,6] #subset the data
  out.matrix[which(ph.dat <= fuz.params[6,1] | ph.dat > fuz.params[6,3] ),6]<- 0.05
  out.matrix[which(ph.dat > fuz.params[6,1] & ph.dat <= fuz.params[6,2]) ,6] <-   (((ph.dat[which(ph.dat > fuz.params[6,1] & ph.dat < fuz.params[6,2])] - fuz.params[6,1]) / (fuz.params[6,2] - fuz.params[6,1]))*0.95) +0.05
  out.matrix[which(ph.dat > fuz.params[6,2] & ph.dat <= fuz.params[6,3]) ,6] <-   (((fuz.params[6,3] - ph.dat[which(ph.dat > fuz.params[6,2] & ph.dat <= fuz.params[6,3])]) / (fuz.params[6,3] - fuz.params[6,2]))*0.95) +0.05
  
  
  #rainfall
  rain.dat<- samp.matrix[,7] #subset the data
  out.matrix[which(rain.dat < fuz.params[7,1]),7]<- 1
  out.matrix[which(rain.dat >= fuz.params[7,1] & rain.dat <= fuz.params[7,2]) ,7] <-   (((fuz.params[7,2] - rain.dat[which(rain.dat >= fuz.params[7,1] & rain.dat <= fuz.params[7,2])] ) / (fuz.params[7,2] - fuz.params[7,1]))*0.95) +0.05
  out.matrix[which(rain.dat > fuz.params[7,2]),7]<- 0.05
  
  
  #soil depth
  out.matrix[which(samp.matrix[,13] == 0),8]<- 1
  out.matrix[which(samp.matrix[,13] != 0 & samp.matrix[,8] > fuz.params[8,2] ),8]<- 1
  out.matrix[which(samp.matrix[,13] != 0 & samp.matrix[,8] >= fuz.params[8,1] & samp.matrix[,8] <= fuz.params[8,2]),8]<- (((samp.matrix[which(samp.matrix[,13] != 0 & samp.matrix[,8] >= fuz.params[8,1] & samp.matrix[,8] <= fuz.params[8,2]),8] - fuz.params[8,1]) / (fuz.params[8,2] - fuz.params[8,1]))*0.95) +0.05
  out.matrix[which(samp.matrix[,13] != 0 & samp.matrix[,8] < fuz.params[8,1] ),8]<- 0.05
  
  # temperature
  temp.dat<- samp.matrix[,9] #subset the data
  out.matrix[which(temp.dat < fuz.params[9,1] | temp.dat > fuz.params[9,4] ),9]<- 0.05
  out.matrix[which(temp.dat >= fuz.params[9,1] & temp.dat < fuz.params[9,2]) ,9] <-   (((temp.dat[which(temp.dat >= fuz.params[9,1] & temp.dat < fuz.params[9,2])] - fuz.params[9,1]) / (fuz.params[9,2] - fuz.params[9,1]))*0.95) +0.05
  out.matrix[which(temp.dat > fuz.params[9,3] & temp.dat < fuz.params[9,4]) ,9] <-   (((fuz.params[9,4] - temp.dat[which(temp.dat > fuz.params[9,3] & temp.dat < fuz.params[9,4])]) / (fuz.params[9,4] - fuz.params[9,3]))*0.95) +0.05
  out.matrix[which(temp.dat >= fuz.params[9,2] & temp.dat <= fuz.params[9,3] ),9]<- 1
  
  # rocks
  out.matrix[which(samp.matrix[,11] == 0),10]<- 1
  out.matrix[which(samp.matrix[,11] != 0 & samp.matrix[,12] > fuz.params[10,2] ),10]<- 0.05
  out.matrix[which(samp.matrix[,11] != 0 & samp.matrix[,12] >= fuz.params[10,1] & samp.matrix[,12] <= fuz.params[10,2]),10]<- (((fuz.params[10,2] - samp.matrix[which(samp.matrix[,11] != 0 & samp.matrix[,12] >= fuz.params[10,1] & samp.matrix[,12] <= fuz.params[10,2]),12] ) / (fuz.params[10,2] - fuz.params[10,1]))*0.95) +0.05
  out.matrix[which(samp.matrix[,11] != 0 & samp.matrix[,12] < fuz.params[10,1] ),10]<- 1
  return(out.matrix)}


 