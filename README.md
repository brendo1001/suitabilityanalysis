# README #

Some development scripts for conducting enterprise suitability analysis.

1. A Naive analysis that considers inputs to be error free
2. An analysis that takes into consideration the underlying uncertainties of the input variables. This approach is described in [Malone et al. (2015)](https://peerj.com/articles/1366/)
3. An extension of the Malone et al. (2015) approach that considers the suitability threshold criteria as continuous membership functions.

Email: brendan.malone@sydney.edu.au